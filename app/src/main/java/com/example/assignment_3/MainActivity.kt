package com.example.assignment_3

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.example.assignment_3.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var listOfEditText: List<EditText>
    private var binding: ActivityMainBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        init()
    }
    // ვერ გადავწყვიტე რომელში დამეწერა  onPause, თუ onStop შედეგი იგივე აქვს,
    // არსებითი მნიშვნელობა ჩემთვის არ აქვს, დავტესტე,
    // მაგრამ რომელში ჯობია?
    override fun onPause() {
        super.onPause()
        makeAllFieldsEmpty(this.listOfEditText)
    }

    private fun isAllFieldsValid(editTexts: List<EditText>): Boolean {
        return editTexts.any { it.text.isEmpty() }
    }

    private fun makeAllFieldsEmpty(editTexts: List<EditText>) {
        editTexts.forEach { it.text.clear() }
    }

    private fun isEmailValid(email: String): Boolean =
        android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()

    private fun makeValidation() {
        when {
            isAllFieldsValid(this.listOfEditText) -> callToast(getString(R.string.fill_all_fields))
            !isEmailValid(binding!!.emailAddress.text.toString()) -> callToast(getString(R.string.email_not_valid))
            binding!!.userName.text.toString().trim().length < 10 -> callToast(getString(R.string.username_not_valid))
            else -> callToast(getString(R.string.everything_saved))
        }
        //  რადგან inputType = number - არის თუ არა რიცხვი, მთელი რიცხვი თუ არის აღარ ვამოწმებ.
        //        რადგან ასაკთან არის დაკავშირებული
        //        მაგრამ მარტივად შეიძლებოდა
        //        !binding!!.age.text.toString().isDigitsOnly()
    }

    private fun callToast(type: String) =
        Toast.makeText(this, type, Toast.LENGTH_SHORT).show()

    private val saveClick = View.OnClickListener {
        makeValidation()
    }

    private fun init() {
        listOfEditText = listOf(
            binding!!.emailAddress,
            binding!!.userName,
            binding!!.firstName,
            binding!!.lastName,
            binding!!.age,
        )

        binding!!.save.setOnClickListener(saveClick)
        binding!!.clear.setOnLongClickListener {
            makeAllFieldsEmpty(this.listOfEditText)
            callToast(getString(R.string.everything_cleared))
            true
        }
    }
}